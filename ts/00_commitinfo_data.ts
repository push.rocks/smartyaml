/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartyaml',
  version: '3.0.1',
  description: 'handle yaml in smart ways'
}
