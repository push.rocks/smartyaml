import * as plugins from './smartyaml.plugins.js';

export let yamlStringToObject = async (
  yamlStringArg,
  optionsArg: plugins.jsYaml.LoadOptions = {}
): Promise<any> => {
  return plugins.jsYaml.load(yamlStringArg);
};

export let objectToYamlString = async (
  objectArg,
  optionsArg: plugins.jsYaml.DumpOptions = {}
): Promise<string> => {
  return plugins.jsYaml.dump(objectArg);
};
